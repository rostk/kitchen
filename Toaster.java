package com.company;

import java.util.LinkedList;
import java.util.List;

public class Toaster extends KitchenAppliance {

    private float CookingTime;

    public Toaster(double Power, String name, String Compatibility, DishesType dishes, float cookingTime) {
        super(Power, name, Compatibility, dishes);
        CookingTime = cookingTime;
    }

    public float getCookingTime() {
        return CookingTime;
    }

    public void setCookingTime(float cookingTime) {
        CookingTime = cookingTime;
    }

    @Override
    public String toString() {
        return "Toaster: " +
                " CookingTime = " + CookingTime +
                " " + super.toString() + "\n";
    }
}
