package com.company;
import java.util.List;
public class Oven extends KitchenAppliance {
    private int temperature;


    public Oven(double Power, String name, String Compatibility, DishesType dishes, int temperature) {
        super(Power, name, Compatibility, dishes);
        this.temperature = temperature;
    }

    public int getTemperature() {
        return temperature;
    }

    public void setTemperature(int temperature) {
        this.temperature = temperature;
    }

    @Override
    public String toString() {
        return "Oven: " +
                "temperature = " + temperature +
                " " + super.toString() + "\n";
    }
}
