package com.company;
import java.util.List;
public class Juicer extends KitchenAppliance {
    private double capacity;

    public Juicer(double Power, String name, String Compatibility, DishesType dishes, double capacity) {
        super(Power, name, Compatibility, dishes);
        this.capacity = capacity;
    }

    public double getCapacity() {
        return capacity;
    }

    public void setCapacity(double capacity) {
        this.capacity = capacity;
    }

    @Override
    public String toString() {
        return "Juicer: " +
                "capacity = " + capacity +
                " " + super.toString() + "\n";
    }
}