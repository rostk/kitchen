package com.company;

import java.util.List;

public class Mixer extends KitchenAppliance {
    private int rotate;

    public Mixer(double Power, String name, String Compatibility, DishesType dishes, int rotate) {
        super(Power, name, Compatibility, dishes);
        this.rotate = rotate;
    }

    public int getRotate() {
        return rotate;
    }

    public void setRotate(int rotate) {
        this.rotate = rotate;
    }

    @Override
    public String toString() {
        return "Mixer: " +
                "rotate = " + rotate +
                " " + super.toString() + "\n";
    }
}
