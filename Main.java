package com.company;
import javafx.scene.transform.Rotate;

import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;
public class Main {

    public static void main(String[] args) {
        KitchenManager manager = new KitchenManager();

        manager.addAppliance(new Toaster(16.23, "Toaster", "Bread",  DishesType.BREADPRODUCTS, 13));
        manager.addAppliance(new Mixer(50.45, "Mixer", "Cakes", DishesType.CAKES, 13));
        manager.addAppliance(new Juicer(100.32, "Juicer", "Juice", DishesType.JUICE, 12));
        manager.addAppliance(new Oven(60.42, "Oven", "Bakery",  DishesType.DESERTS, 90));
        manager.addAppliance(new Oven(160.23, "Oven123", "Bakery",  DishesType.DESERTS, 200));

        System.out.println("1 - Sort By Power");
        System.out.println("2 - Find By Compatibility");
        Scanner sc=new Scanner (System.in);
        int choice=sc.nextInt();

        switch(choice){
            case 1:  System.out.println(manager.sortByPower());
            break;
            case 2:  System.out.println(manager.findByCompatibility("Bakery"));
            break;
        }









        }
    }


