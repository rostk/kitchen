package com.company;
import java.util.LinkedList;
import java.util.List;


public class KitchenAppliance  {
    private double Power;
    private String Compatibility;
    private DishesType Dishes;
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public KitchenAppliance(double Power, String name, String Compatibility, DishesType dishes) {
        this.Power = Power;
        this.name = name;
        this.Compatibility = Compatibility;
        this.Dishes=dishes;
    }

    public String getCompatibility() {
        return Compatibility;
    }

    public double getPower() {
        return Power;
    }

    public void setPower(double power) {
        Power = power;
    }

    @Override
    public String toString() {
        return "Power = " + Power +
                " Compatibility = " + Compatibility +
                " Dishes = " + Dishes +
                " name = " + name;
    }
}