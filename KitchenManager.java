package com.company;

import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

public class KitchenManager {
    private List<KitchenAppliance> Appliance = new LinkedList();
    private String FindByCompatibility;

    public void setAppliance(List<KitchenAppliance> appliance) {
        Appliance = appliance;

    }


    public void addAppliance(KitchenAppliance appliance) {
        Appliance.add(appliance);
    }


    public List<KitchenAppliance> sortByPower() {
        Appliance.sort(Comparator.comparingDouble(KitchenAppliance::getPower));
        return Appliance;
    }

    /*  public List<KitchenAppliance> sortBy() {
        Appliance.sort(KitchenAppliance o1, KitchenAppliance o2)->o1.getPower()-o2.getPower());
          return Appliance;
          }
     */
    public List<KitchenAppliance> findByCompatibility(String FindByCompatibility) {
        List<KitchenAppliance> result = new LinkedList<KitchenAppliance>();
        for (KitchenAppliance app : Appliance) {
            if (FindByCompatibility == app.getCompatibility()) {
                result.add(app);
            }
        }
        return result;
    }


    public List<KitchenAppliance> getAppliance() {
        return Appliance;
    }


}
